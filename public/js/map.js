var map = L.map('map').setView([51.2171918, 4.415], 14);
var userLatLng;

var Stamen_TonerLite = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.{ext}', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	subdomains: 'abcd',
	minZoom: 0,
	maxZoom: 18,
	ext: 'png'
}).addTo(map);

//add location
var lc = L.control.locate({
    locateOptions: {
        enableHighAccuracy: true
    },
    position: 'topright',
    strings: {
        title: "Waar ben ik!"
    },
}).addTo(map);

lc.start();

map.on('locationfound', onLocationFound);

function onLocationFound(e) {
    // create a marker at the users "latlng" and add it to the map
    userLatLng = e.latlng;
    console.log(userLatLng);
}

//toilet icons
var toiletMixedClosed = L.icon({
    iconUrl: 'img/toiletMixedClosed.png',
    shadowUrl: 'img/markers/markers-shadow-round.png',

    iconSize:     [50, 50], // size of the icon
    shadowSize:   [110, 55], // size of the shadow
    iconAnchor:   [10, 5], // point of the icon which will correspond to marker's location
    shadowAnchor: [22, 15],  // the same for the shadow
    popupAnchor:  [15, 0] // point from which the popup should open relative to the iconAnchor
});

var toiletMixedOpen = L.icon({
    iconUrl: 'img/toiletMixedOpen.png',
    shadowUrl: 'img/markers/markers-shadow-round.png',

    iconSize:     [50, 50], // size of the icon
    shadowSize:   [110, 55], // size of the shadow
    iconAnchor:   [10, 5], // point of the icon which will correspond to marker's location
    shadowAnchor: [22, 15],  // the same for the shadow
    popupAnchor:  [15, 0] // point from which the popup should open relative to the iconAnchor
});

var toiletManClosed = L.icon({
    iconUrl: 'img/toiletManClosed.png',
    shadowUrl: 'img/markers/markers-shadow-round.png',

    iconSize:     [50, 50], // size of the icon
    shadowSize:   [110, 55], // size of the shadow
    iconAnchor:   [10, 5], // point of the icon which will correspond to marker's location
    shadowAnchor: [22, 15],  // the same for the shadow
    popupAnchor:  [15, 0] // point from which the popup should open relative to the iconAnchor
});

var toiletManOpen = L.icon({
    iconUrl: 'img/toiletManOpen.png',
    shadowUrl: 'img/markers/markers-shadow-round.png',

    iconSize:     [50, 50], // size of the icon
    shadowSize:   [110, 55], // size of the shadow
    iconAnchor:   [10, 5], // point of the icon which will correspond to marker's location
    shadowAnchor: [22, 15],  // the same for the shadow
    popupAnchor:  [15, 0] // point from which the popup should open relative to the iconAnchor
});

var locationMarker = L.AwesomeMarkers.icon({icon: 'flag', prefix: 'fa', markerColor: 'blue', spin:false});