//Entry point applicatie

//toevoegen libraries
const express = require('express');
const path = require('path');
const request = require('request');

//onze app werkt op express (webservice)
const app = express();

//als we geen poort in het proces vinden, kies dan poort 3000
const port = process.env.PORT || 3000;

var data = null;

//De app gebruikt de statische map public om zijn publieke resources zoals afbeeldingen, css, js uit te halen
app.use(express.static('public'));

//We zetten de view engine op ejs, op deze manier weet de app dat we voor views die gebruiker ziet alleen maar ejs files gaan gebruiken
app.set('view engine', 'ejs');

//we zetten het pad naar de views, en zoeken naar de map met de naam views
app.set('views', path.resolve(__dirname, 'views'));

//we halen de data op van de geodata site van Antwerpen
request('https://geodata.antwerpen.be/arcgissql/rest/services/P_Portal/portal_publiek1/MapServer/8/query?where=1%3D1&outFields=OMSCHRIJVING,TYPE,BETALEND,DOELGROEP,LUIERTAFEL,MABU,MAEU,DIBU,DIEU,WOBU,WOEU,DOBU,DOEU,VRBU,VREU,ZABU,ZAEU,ZOBU,ZOEU,X_COORD,OBJECTID,CATEGORIE,STRAAT,HUISNUMMER,OPENINGSUREN_OPM,Y_COORD&outSR=4326&f=json',
    function(error, response, body){
        data = JSON.parse(body);
    }
);

app.get('/', function (req, res) {
    res.render('home', {
        toiletArray: data.features
    });
});

app.get('/contact', function (req, res) {
    res.render('contact');
});

app.listen(port, function() {
    console.log('Node luistert op poort 3000');
});
